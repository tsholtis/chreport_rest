# base node image
FROM node:12.13.0

# set working directory
WORKDIR /app

# add node_modules to the path
ENV PATH /app/node_modules/.bin:$PATH

# install python
RUN ["apt-get", "update"]
RUN ["apt-get", "upgrade", "-y", "--fix-missing"]
RUN ["apt-get", "install", "-y", "python3"]
RUN ["apt-get", "install", "-y", "python3-pip"]
RUN ["apt-get", "install", "-y", "python3-venv"]
RUN ["apt-get", "install", "-y", "git"]

# install node dependencies
COPY package.json .
RUN npm install

# copy the project
COPY . .

# install the app
RUN ["chmod", "+x", "./install.sh"]
RUN ["./install.sh"]
RUN ["chmod", "+x", "bin/call_chreport.sh"]

# run the app
CMD node server.js
