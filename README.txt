This is the REST service portion that stores uploaded files and calls the ch_report program to process the raw data 
into a report, then replies to the calling client with a link to the report.

Technology used here is Python3 and ExpressJS.

Deployment:
git clone https://bitbucket.org/tsholtis/chreport_rest/

Installation:
npm install
chmod +x ./bin/call_chreport.sh