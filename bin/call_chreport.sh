#!/bin/bash

# setup the environment
# TODO: Change the CHREPORT_ROOT to get the path from a property that is setup on install (unzip directory)
export CHREPORT_ROOT=./exe
export VENV_DIR=$CHREPORT_ROOT/venv

# enable env
source $VENV_DIR/bin/activate

# call ch_report
python -m main.main -sourceDir=$1 -sourceFiles=$2 -reportFile=$3