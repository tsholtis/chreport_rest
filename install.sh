#!/bin/bash

# setup environment
export CHREPORT_ROOT=.
mkdir -p $CHREPORT_ROOT/bin $CHREPORT_ROOT/data $CHREPORT_ROOT/exe
export CHREPORT_BIN=$CHREPORT_ROOT/bin
export CHREPORT_EXE=$CHREPORT_ROOT/exe

# install dependencies
apt-get install libxml2-dev
apt-get install libxslt-dev
apt-get install zlib1g-dev

# setup data locations
mkdir -p $CHREPORT_ROOT/data/in $CHREPORT_ROOT/data/out

# setup the virtual environment for chreport
git clone https://tsholtis@bitbucket.org/tsholtis/ch_report.git $CHREPORT_EXE
mkdir -p $CHREPORT_EXE/venv
export CHREPORT_VENV=$CHREPORT_EXE/venv
python3 -m venv $CHREPORT_VENV
source $CHREPORT_VENV/bin/activate

# run the installation
export PYTHONPATH=$PYTHONPATH:$CHREPORT_VENV/lib/python3.6
pip3 install wheel
pip3 install $CHREPORT_EXE






