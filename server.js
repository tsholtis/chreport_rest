const express = require('express'),
    path = require('path'),
    cors = require('cors'),
    multer = require('multer'),
    bodyParser = require('body-parser')
    appRoot = require('app-root-path');

// file upload settings
const DATA_PATH = appRoot + path.sep + 'data';
const IN_PATH = DATA_PATH + path.sep + 'in';
const OUT_PATH= DATA_PATH + path.sep + 'out';
const FORM = 'fileupload';
const FILE_COUNT_LIMIT = 12;

// report executer settings
const EXE_PATH = appRoot + path.sep + 'bin';
const REPORT_EXECUTOR = 'call_chreport.sh';

// report settings
const DEFAULT_REPORT_NAME = "default_report";

// multer settings
let storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, IN_PATH);
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname+Date.now()+".xlsx");
    }
});

let upload = multer({
    storage: storage
})

// express settings
const app = express();
app.use('*', cors({
    credentials: true,
})); // pre-flight for HTTP

app.use(function(req, res, next) {
    console.log("Setting up CORS headers...");
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

// get a file
app.get(
    '/chreport/download/:file(*)',
    function(req, res) {
        let file = req.params.file;
        let fileLocation = path.join(OUT_PATH, file);
        console.log("file location = " + fileLocation);
        res.download(fileLocation, file);
    }
);

// post a file = store to PATH
app.post(
    '/chreport/upload',
    upload.array(FORM, FILE_COUNT_LIMIT), 
    function (req, res, next) {
        console.log(new Date() + " - " + "New request incoming...");
        if (!req.files) {
            console.log("No file is available.");
            next(req, res);
        }
        else {
            console.log("File is available.");
            var reportName = OUT_PATH; 
            if(req.body.reportName) {
                reportName += path.sep + req.body.reportName;
                if (!reportName.endsWith('.xlsx')) {
                    reportName += ".xlsx";
                }
            }
            else {
                reportName += path.sep + DEFAULT_REPORT_NAME + Date.now() + ".xlsx"; 
            }
           
            sourceFiles = [];
            for (let i in req.files) {
                sourceFiles.push(req.files[i].filename);
            }

            // call ch_report program 
            const spawn = require("child_process").spawn;          
            const process = spawn(EXE_PATH+path.sep+REPORT_EXECUTOR, [IN_PATH, sourceFiles.toString(), reportName]);
            console.log("process spawned - calling report");
            console.log("command run = " + EXE_PATH+path.sep+REPORT_EXECUTOR+" -datadir="+IN_PATH+" -files="+sourceFiles.toString()+" -filename="+reportName);
            process.on("close", function(code) {
                // this backtick ` enclosed string is a template string supported in nodejs v4, using ES6 template strings
                console.log(`Reporting script exited with code ${code}`); 
                if (code == 0) {
                    console.log("Successful report call.");
                    // res.set("Access-Control-Allow-Origin", req.hostname);
                    res.write(path.join('/chreport/download/',req.body.reportName + '.xlsx'));  // TODO: Really need to fix this setup - should parse out extension, name, and path earlier and use them where needed
                }
                else {
                    console.log("Error calling reporting function.");
                    res.write("Error - could not call report function.");
                }
                res.end();
            });
        }
    }
);

// setup listening service
const PORT = process.env.PORT || 8080;
const server = app.listen(PORT, () => {
    console.log("Connected to port " + PORT);
});

// 404 handling
app.use((req, res, next) => {
    console.log(new Date() + " - " + "New request incoming...");
    next();
});

function createError(s) {
    return new Error("Page could not be found: "+s);
};

// error handler
app.use(function(err, req, res, next) {
    console.log("An Error was detected....!!");
    console.error(err.message);
    if (!err.statusCode) {
        err.statusCode = 500;
    }
    console.log("setting error code and message.");
    res.status(err.statusCode);
    res.message = err.message;
    console.log("set error code and message.");
    res.end();
});